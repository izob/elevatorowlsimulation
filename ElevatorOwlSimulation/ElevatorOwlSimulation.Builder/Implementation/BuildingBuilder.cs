using System;
using ElevatorOwlSimulation.Builder.Abstraction;
using ElevatorOwlSimulation.Domain.Interfaces;
using ElevatorOwlSimulation.Domain.Models;

namespace ElevatorOwlSimulation.Builder.Implementation
{
    public abstract class BuildingBuilder : IBuildingBuilder<int, IElevator<int, Guid>, IFlour<int, Guid>,
        IBuilding<int, IElevator<int, Guid>, IFlour<int, Guid>>>
    {
        private IBuilding<int, IElevator<int, Guid>, IFlour<int, Guid>> Building = new Building();
        protected BuildingParameter BuildingParameters;

        public void CreateBuilding(BuildingParameter parameters = null)
        {
            if (parameters == null)
            {
                BuildingParameters = new BuildingParameter();
            }
            else
            {
                BuildingParameters = parameters;
            }

            for (var i = 0; i < BuildingParameters.FlourCount; i++)
            {
                this.CreateFlour(ref this.Building);
            }

            for (var i = 0; i < BuildingParameters.ElevatorCount; i++)
            {
                this.CreateElevator(ref this.Building);
            }

            this.Building.MaxFlour = this.Building.Flours.Count - 1;
        }

        public IBuilding<int, IElevator<int, Guid>, IFlour<int, Guid>> GetBuilding()
        {
            return this.Building;
        }

        public abstract void CreateFlour(ref IBuilding<int, IElevator<int, Guid>, IFlour<int, Guid>> building);
        public abstract void CreateElevator(ref IBuilding<int, IElevator<int, Guid>, IFlour<int, Guid>> building);
    }
}