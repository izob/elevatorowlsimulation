using ElevatorOwlSimulation.Domain.Interfaces;
using ElevatorOwlSimulation.Domain.Models;

namespace ElevatorOwlSimulation.Builder.Abstraction
{
    public interface IBuildingBuilder<in TId, in TElevator, in TFlour, out TBuilding>
        where TBuilding : IBuilding<TId, TElevator, TFlour>
    {
        void CreateBuilding(BuildingParameter parameters);
        TBuilding GetBuilding();
    }
}