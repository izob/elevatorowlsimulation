using System.Collections.Generic;
using ElevatorOwlSimulation.Domain.Enums;
using ElevatorOwlSimulation.Domain.Interfaces.Core;

namespace ElevatorOwlSimulation.Domain.Interfaces
{
    public interface IElevator<TId,TPId>: IBaseModel<TId>
    {
        int? NextFlour { get; set; }
        int? CurrentFlour { get; set; }
        DirectionType Direction { get; set; }
        ICollection<IPerson<TPId>> People { get; set; }
    }
}