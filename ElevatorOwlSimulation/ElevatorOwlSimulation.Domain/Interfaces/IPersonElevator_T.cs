using ElevatorOwlSimulation.Domain.Interfaces.Core;

namespace ElevatorOwlSimulation.Domain.Interfaces
{
    public interface IPersonElevator<TId, TPId> : IBaseModel<TId>
    {
        IPerson<TPId> Person { get; set; }
        TId ElevatorId { get; set; }
    }
}