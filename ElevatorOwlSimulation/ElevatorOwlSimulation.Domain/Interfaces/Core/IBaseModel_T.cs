using System.Threading;

namespace ElevatorOwlSimulation.Domain.Interfaces.Core
{
    public interface IBaseModel<TId>
    {
        TId Id { get; set; }
    }
}