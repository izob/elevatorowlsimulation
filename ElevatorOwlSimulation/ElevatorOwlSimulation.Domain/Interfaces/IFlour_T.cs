using System.Collections.Generic;
using ElevatorOwlSimulation.Domain.Interfaces.Core;

namespace ElevatorOwlSimulation.Domain.Interfaces
{
    public interface IFlour<TId, TPId> : IBaseModel<TId>
    {
        int Number { get; set; }
        ICollection<IPersonElevator<TId, TPId>> ElevatorsQueue { get; set; }
    }
}