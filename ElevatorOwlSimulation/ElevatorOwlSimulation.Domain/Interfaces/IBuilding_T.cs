using System.Collections.Generic;
using ElevatorOwlSimulation.Domain.Interfaces.Core;

namespace ElevatorOwlSimulation.Domain.Interfaces
{
    public interface IBuilding<TId, TElevator, TFlour> : IBaseModel<TId>
    {
        ICollection<TElevator> Elevators { get; set; }
        ICollection<TFlour> Flours { get; set; }
        int MaxFlour { get; set; }
    }
}