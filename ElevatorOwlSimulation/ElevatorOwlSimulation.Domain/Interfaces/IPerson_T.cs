using ElevatorOwlSimulation.Domain.Enums;
using ElevatorOwlSimulation.Domain.Interfaces.Core;

namespace ElevatorOwlSimulation.Domain.Interfaces
{
    public interface IPerson<TId> : IBaseModel<TId>
    {
        int InFlour { get; set; }
        int OutFlour { get; set; }

        DirectionType Direction { get; set; }
    }
}