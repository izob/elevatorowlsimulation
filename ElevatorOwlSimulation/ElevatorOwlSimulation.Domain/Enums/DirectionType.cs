namespace ElevatorOwlSimulation.Domain.Enums
{
    public enum DirectionType
    {
        Up = 1, 
        Down = 2,
        Stop = 3
    }
}