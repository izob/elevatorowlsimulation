using ElevatorOwlSimulation.Domain.Models.Static;

namespace ElevatorOwlSimulation.Domain.Models
{
    public class BuildingParameter
    {
        public int FlourCount { get; set; }
        public int ElevatorCount { get; set; }
        public float GeneratePersonTimeout { get; set; }
        public float ElevatorMaxWeight { get; set; }

        public BuildingParameter()
        {
            FlourCount = BuildingConstants.StaticFloursCount;
            ElevatorCount = BuildingConstants.StaticElevatorsCount;
            GeneratePersonTimeout = BuildingConstants.StaticGeneratePersonTimeout;
            ElevatorMaxWeight = BuildingConstants.ElevatorMaxWeight;
        }
    }
}