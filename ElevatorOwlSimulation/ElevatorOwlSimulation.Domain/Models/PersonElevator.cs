using System;
using ElevatorOwlSimulation.Domain.Interfaces;

namespace ElevatorOwlSimulation.Domain.Models
{
    public class PersonElevator: IPersonElevator<int, Guid>
    {
        public int Id { get; set; }
        public IPerson<Guid> Person { get; set; }
        public int ElevatorId { get; set; }
    }
}