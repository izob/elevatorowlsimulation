using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using ElevatorOwlSimulation.Domain.Interfaces;

namespace ElevatorOwlSimulation.Domain.Models
{
    public class Flour : IFlour<int, Guid>
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public ICollection<IPersonElevator<int, Guid>> ElevatorsQueue { get; set; }
    }
}