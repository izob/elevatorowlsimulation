namespace ElevatorOwlSimulation.Domain.Models.Static
{
    public static class BuildingConstants
    {
        public static readonly int StaticFloursCount = 7;
        public static readonly int StaticElevatorsCount = 2;
        public static readonly int StaticGeneratePersonTimeout = 5;
        public static readonly float ElevatorMaxWeight = 150;
    }
}