using System;
using ElevatorOwlSimulation.Domain.Enums;
using ElevatorOwlSimulation.Domain.Interfaces;

namespace ElevatorOwlSimulation.Domain.Models
{
   
    public class Person : IPerson<Guid>
    {
        public Guid Id { get; set; }
        public float Weight { get; set; }
        public int InFlour { get; set; }
        public int OutFlour { get; set; }
        public DirectionType Direction { get; set; }

        public Person() { }

        public Person(IPerson<Guid> person)
        {
            Id = person.Id;
            InFlour = person.InFlour;
            OutFlour = person.OutFlour;
            Direction = person.Direction;
        }
    }
}