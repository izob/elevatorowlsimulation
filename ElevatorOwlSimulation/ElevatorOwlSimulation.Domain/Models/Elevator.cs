using System;
using System.Collections.Generic;
using ElevatorOwlSimulation.Domain.Enums;
using ElevatorOwlSimulation.Domain.Interfaces;

namespace ElevatorOwlSimulation.Domain.Models
{
    public class Elevator : IElevator<int, Guid>
    {
        public int Id { get; set; }
        public int? NextFlour { get; set; }
        public int? CurrentFlour { get; set; }
        public DirectionType Direction { get; set; }
        public ICollection<IPerson<Guid>> People { get; set; }
        public float MaxWeight { get; set; }
    }
}