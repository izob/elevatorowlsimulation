using System;
using System.Collections.Generic;
using ElevatorOwlSimulation.Domain.Interfaces;

namespace ElevatorOwlSimulation.Domain.Models
{
    public class Building: IBuilding<int, IElevator<int, Guid>, IFlour<int, Guid>>
    {
        public int Id { get; set; }
        public ICollection<IElevator<int, Guid>> Elevators { get; set; }
        public ICollection<IFlour<int, Guid>> Flours { get; set; }
        public int MaxFlour { get; set; }
    }
}