using System;
using ElevatorOwlSimulation.Domain.Interfaces;
using ElevatorOwlSimulation.Domain.Models;
using ElevatorOwlSimulation.Service.Abstraction;

namespace ElevatorOwlSimulation.Service.Implementation
{
    public class OwlBuilderManager: IOwlBuilderManager
    {
        private readonly IOwlBuildingBuilder _owlBuildingBuilder;

        public OwlBuilderManager(IOwlBuildingBuilder owlBuildingBuilder)
        {
            _owlBuildingBuilder = owlBuildingBuilder;
        }
        
        public IBuilding<int, IElevator<int, Guid>, IFlour<int, Guid>> GetOwlBuilding(BuildingParameter buildingParameters = null)
        {
            this._owlBuildingBuilder.CreateBuilding(buildingParameters);
            return _owlBuildingBuilder.GetBuilding();
        }
    }
}