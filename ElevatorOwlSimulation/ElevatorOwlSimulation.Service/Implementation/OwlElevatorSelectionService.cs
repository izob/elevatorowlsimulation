using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using ElevatorOwlSimulation.Domain.Interfaces;
using ElevatorOwlSimulation.Domain.Models;
using ElevatorOwlSimulation.Service.Abstraction;

namespace ElevatorOwlSimulation.Service.Implementation
{
    public class OwlElevatorSelectionService : IOwlElevatorSelectionService
    {
        public int ChoosePersonElevator(ref IBuilding<int, IElevator<int, Guid>, IFlour<int, Guid>> building, IPerson<Guid> person)
        {
            IFlour<int, Guid> first = null;
            foreach (var x in building.Flours)
            {
                if (x.Number == person.InFlour)
                {
                    first = x;
                    break;
                }
            }

            if (first == null)
            {
                throw new NullReferenceException();
            }

            // ReSharper disable once PossibleNullReferenceException
            
            var orderingElevatorsByPeopleCount = first.ElevatorsQueue.GroupBy(x => x.ElevatorId)
                .OrderBy(x => x.Count())
                .Select(x=>x.Key);


            var elevatorCollectionCorrectdDirection = building.Elevators.Where(x => x.Direction == person.Direction)
                .OrderByDescending(x => x.Id)
                .Select(x => x.Id)
                .Concat(building.Elevators.Where(x => x.Direction != person.Direction).OrderByDescending(x => x.Id).Select(y=>y.Id));

            if (!orderingElevatorsByPeopleCount.Any())
            {
                orderingElevatorsByPeopleCount = elevatorCollectionCorrectdDirection;
            }
            var choosenElevatorId = orderingElevatorsByPeopleCount?.Select(x => new
            {
                ElevatorId = x,
                Rating = orderingElevatorsByPeopleCount.ToList().IndexOf(x)  +
                         elevatorCollectionCorrectdDirection.ToList().IndexOf(x)
            }).OrderByDescending(x=>x.Rating).FirstOrDefault().ElevatorId;

            building.Flours
                .FirstOrDefault(x => x.Number == person.InFlour)?.ElevatorsQueue
                .Add(new PersonElevator
                {
                    ElevatorId = choosenElevatorId.Value, 
                    Person = person
                });
            return choosenElevatorId.Value;
        }
    }
}