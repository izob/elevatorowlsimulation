using System;
using System.Linq;
using ElevatorOwlSimulation.Domain.Interfaces;
using ElevatorOwlSimulation.Domain.Models;
using ElevatorOwlSimulation.Service.Abstraction;

namespace ElevatorOwlSimulation.Service.Implementation
{
    public class OwlElevatorValidationService: IOwlElevatorValidationService
    {
        public bool Validate(IElevator<int, Guid> elevator, IPerson<Guid> person)
        {
            return elevator.People.Where(x => x.OutFlour != elevator.CurrentFlour).Sum(x => (x as Person).Weight) +
                   (person as Person).Weight > (elevator as Elevator).MaxWeight;
        }
    }
}