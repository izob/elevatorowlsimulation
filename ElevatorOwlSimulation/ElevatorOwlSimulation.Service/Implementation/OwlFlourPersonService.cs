using System;
using System.Linq;
using ElevatorOwlSimulation.Domain.Enums;
using ElevatorOwlSimulation.Domain.Interfaces;
using ElevatorOwlSimulation.Domain.Models;
using ElevatorOwlSimulation.Service.Abstraction;
using ElevatorOwlSimulation.Service.Models;

namespace ElevatorOwlSimulation.Service.Implementation
{
    public class OwlFlourPersonService : IOwlFlourPersonService
    {
        private readonly Random _randomizer;
        private readonly IOwlElevatorSelectionService _owlElevatorSelectionService;
        private readonly IOwlElevatorValidationService _owlElevatorValidationService;

        public OwlFlourPersonService(IOwlElevatorSelectionService owlElevatorSelectionService,
            IOwlElevatorValidationService owlElevatorValidationService)
        {
            _owlElevatorSelectionService = owlElevatorSelectionService;
            _randomizer = new Random();
            _owlElevatorValidationService = owlElevatorValidationService;
        }

        public PersonElevatorModel CreatePerson(ref IBuilding<int, IElevator<int, Guid>, IFlour<int, Guid>> building)
        {
            var person = new Person
            {
                Id = Guid.NewGuid(),
                Weight = (float) Math.Round(_randomizer.Next(15, 120) + _randomizer.NextDouble(), 2),
                InFlour = _randomizer.Next(0, building.MaxFlour + 1),
                OutFlour = -1,
            };

            while (person.OutFlour == person.InFlour || person.OutFlour == -1)
            {
                person.OutFlour = _randomizer.Next(0, building.MaxFlour + 1);
            }

            person.Direction = person.InFlour > person.OutFlour ? DirectionType.Down : DirectionType.Up;
            var elevatorId = _owlElevatorSelectionService.ChoosePersonElevator(ref building, person);
            return new PersonElevatorModel()
            {
                ElevatorId = elevatorId,
                Person = person
            };
        }
    }
}