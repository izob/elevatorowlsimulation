using System;
using System.Collections.Generic;
using ElevatorOwlSimulation.Builder.Implementation;
using ElevatorOwlSimulation.Domain.Enums;
using ElevatorOwlSimulation.Domain.Interfaces;
using ElevatorOwlSimulation.Domain.Models;
using ElevatorOwlSimulation.Service.Abstraction;

namespace ElevatorOwlSimulation.Service.Implementation
{
    public class OwlBuildingBuilder:BuildingBuilder, IOwlBuildingBuilder
    {
        public override void CreateFlour(ref IBuilding<int, IElevator<int, Guid>, IFlour<int, Guid>> building)
        {
            if (building.Flours == null)
            {
                building.Flours = new List<IFlour<int, Guid>>();
            }
            
            building.Flours.Add(new Flour
            {
                Id = building.Flours.Count, 
                Number = building.Flours.Count,
                ElevatorsQueue = new List<IPersonElevator<int, Guid>>()
            });
        }

        public override void CreateElevator(ref IBuilding<int, IElevator<int, Guid>, IFlour<int, Guid>> building)
        {
            if (building.Elevators == null)
            {
                building.Elevators = new List<IElevator<int, Guid>>();
            }
            
            building.Elevators.Add(new Elevator
            {
                Id = building.Elevators.Count, 
                CurrentFlour = building.MaxFlour, 
                NextFlour = null, 
                Direction = DirectionType.Stop, 
                People = new List<IPerson<Guid>>(), 
                MaxWeight = this.BuildingParameters.ElevatorMaxWeight
            });
        }

        
    }
}