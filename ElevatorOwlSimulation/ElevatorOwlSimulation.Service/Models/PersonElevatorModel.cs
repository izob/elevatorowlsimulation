﻿using ElevatorOwlSimulation.Domain.Models;

namespace ElevatorOwlSimulation.Service.Models
{
    public class PersonElevatorModel
    {
        public int ElevatorId { get; set; }
        public Person Person { get; set; }
    }
}
