using System;
using ElevatorOwlSimulation.Domain.Interfaces;
using ElevatorOwlSimulation.Service.Models;

namespace ElevatorOwlSimulation.Service.Abstraction
{
    public interface IOwlFlourPersonService
    {
        PersonElevatorModel CreatePerson(ref IBuilding<int, IElevator<int, Guid>, IFlour<int, Guid>> building);
    }
}