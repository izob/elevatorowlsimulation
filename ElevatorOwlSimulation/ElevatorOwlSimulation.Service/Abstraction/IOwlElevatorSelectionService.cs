using System;
using ElevatorOwlSimulation.Domain.Interfaces;

namespace ElevatorOwlSimulation.Service.Abstraction
{
    public interface IOwlElevatorSelectionService
    {
        int ChoosePersonElevator(ref IBuilding<int, IElevator<int, Guid>, IFlour<int, Guid>> building, IPerson<Guid> person);
    }
}