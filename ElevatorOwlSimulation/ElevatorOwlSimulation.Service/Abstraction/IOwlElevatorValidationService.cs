using System;
using ElevatorOwlSimulation.Domain.Interfaces;

namespace ElevatorOwlSimulation.Service.Abstraction
{
    public interface IOwlElevatorValidationService
    {
        bool Validate(IElevator<int, Guid> elevator, IPerson<Guid> person);
    }
}