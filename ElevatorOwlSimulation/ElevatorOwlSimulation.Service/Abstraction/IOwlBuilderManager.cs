using System;
using ElevatorOwlSimulation.Domain.Interfaces;
using ElevatorOwlSimulation.Domain.Models;

namespace ElevatorOwlSimulation.Service.Abstraction
{
    public interface IOwlBuilderManager
    {
        IBuilding<int, IElevator<int, Guid>, IFlour<int, Guid>> GetOwlBuilding(BuildingParameter buildingParameters);
    }
}