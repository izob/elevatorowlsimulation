using System;
using ElevatorOwlSimulation.Builder.Abstraction;
using ElevatorOwlSimulation.Domain.Interfaces;

namespace ElevatorOwlSimulation.Service.Abstraction
{
    public interface IOwlBuildingBuilder : IBuildingBuilder<int, IElevator<int, Guid>, IFlour<int, Guid>,
        IBuilding<int, IElevator<int, Guid>, IFlour<int, Guid>>>
    {
    }
}