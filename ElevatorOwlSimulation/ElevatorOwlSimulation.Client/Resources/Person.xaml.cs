﻿using ElevatorOwlSimulation.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ElevatorOwlSimulation.Client.Resources
{
    /// <summary>
    /// Interaction logic for Person.xaml
    /// </summary>
    public partial class Person : UserControl
    {
        public Guid Id { get; set; }
        public int InFlour { get; set; }
        public int OutFlour { get; set; }
        public DirectionType Direction { get; set; }
        public Person()
        {
            InitializeComponent();
        }
    }
}
