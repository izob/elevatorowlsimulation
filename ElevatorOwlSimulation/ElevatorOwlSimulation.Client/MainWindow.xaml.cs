﻿using ElevatorOwlSimulation.Builder.Implementation;
using ElevatorOwlSimulation.Domain.Enums;
using ElevatorOwlSimulation.Domain.Interfaces;
using ElevatorOwlSimulation.Domain.Models;
using ElevatorOwlSimulation.Domain.Models.Static;
using ElevatorOwlSimulation.Service.Abstraction;
using ElevatorOwlSimulation.Service.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ElevatorOwlSimulation.Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public int FloorCount;
        public int ElevatorsCount;

        public Canvas myCanvas;
        public List<Image> elevators = new List<Image>();
        public ICollection<Resources.Person> People = new List<Resources.Person>();
        Random rnd = new Random();
        private IOwlBuilderManager _manager;
        private IOwlBuildingBuilder _buildingBuilder;
        private IBuilding<int, IElevator<int, Guid>, IFlour<int, Guid>> _building;
        private System.Timers.Timer _timer;
        public BuildingParameter _parameter;
        private IOwlElevatorSelectionService _elevatorSelectionService;
        private IOwlElevatorValidationService _owlElevatorValidationService;
        private IOwlFlourPersonService _owlFlourPersonService;
        public MainWindow()
        {
            InitializeComponent();

        }

        private void DrawComponents()
        {

            _building = _manager.GetOwlBuilding(_parameter);
            FloorCount = _building.Flours.Count;
            ElevatorsCount = _building.Elevators.Count;

            foreach (var item in _building.Elevators)
            {
                item.CurrentFlour = _building.Flours.Count;
            }

            Line leftRoofLine = new Line();
            leftRoofLine.X1 = 30;
            leftRoofLine.X2 = (310 + (250 * (ElevatorsCount - 1))) / 2;
            leftRoofLine.Y1 = 130;
            leftRoofLine.Y2 = 50;
            leftRoofLine.Stroke = new SolidColorBrush(Colors.Black);
            leftRoofLine.StrokeThickness = 10;

            Line rightRoofLine = new Line();
            rightRoofLine.X1 = leftRoofLine.X2 - 5;
            rightRoofLine.X2 = 280 + (250 * (ElevatorsCount - 1));
            rightRoofLine.Y1 = 50;
            rightRoofLine.Y2 = 130;
            rightRoofLine.Stroke = new SolidColorBrush(Colors.Black);
            rightRoofLine.StrokeThickness = 10;



            Line leftLine = new Line();
            leftLine.X1 = leftRoofLine.X1 + 20;
            leftLine.X2 = leftRoofLine.X1 + 20;
            leftLine.Y1 = leftRoofLine.Y1;
            leftLine.Y2 = leftRoofLine.Y2 + 120 * FloorCount;
            leftLine.Stroke = new SolidColorBrush(Colors.Black);
            leftLine.StrokeThickness = 10;

            Line rightLine = new Line();
            rightLine.X1 = rightRoofLine.X2 - 20;
            rightLine.X2 = rightRoofLine.X2 - 20;
            rightLine.Y1 = leftRoofLine.Y1;
            rightLine.Y2 = leftRoofLine.Y2 + 120 * FloorCount;
            rightLine.Stroke = new SolidColorBrush(Colors.Black);
            rightLine.StrokeThickness = 10;

            //Image roof = new Image();
            //roof.Stretch = Stretch.Fill;
            //roof.Height = 150;
            //roof.Width = 285 * ElevatorsCount;
            //roof.SetValue(Canvas.LeftProperty, 10.0);
            //roof.SetValue(Canvas.TopProperty, 10.0);
            //roof.Source = new BitmapImage(new Uri(@"./Resources/roof.png", UriKind.RelativeOrAbsolute));


            for (int i = 0; i < ElevatorsCount; i++)
            {
                Image elevator = new Image();
                elevator.Stretch = Stretch.Fill;
                elevator.Height = 80;
                elevator.Width = 80;
                elevator.SetValue(Canvas.LeftProperty, 50.0 + (250 * i));
                elevator.SetValue(Canvas.TopProperty, 150.0);
                elevator.Source = new BitmapImage(new Uri(@"./Resources/elevator.png", UriKind.RelativeOrAbsolute));

                elevators.Add(elevator);

                for (int j = 0; j < FloorCount; j++)
                {
                    Image floor = new Image();
                    floor.Stretch = Stretch.Fill;
                    floor.Height = 80;
                    floor.Width = 80;
                    floor.SetValue(Canvas.LeftProperty, 150.0 + (250 * i));
                    floor.SetValue(Canvas.TopProperty, 150.0 + 100 * j);
                    floor.Source = new BitmapImage(new Uri(@"./Resources/floor-elevator.png", UriKind.RelativeOrAbsolute));
                    myCanvas.Children.Add(floor);
                }

                myCanvas.Children.Add(elevator);
            }

            myCanvas.Children.Add(leftLine);
            myCanvas.Children.Add(rightLine);
            myCanvas.Children.Add(leftRoofLine);
            myCanvas.Children.Add(rightRoofLine);
        }

        private void DrawingCanvas_Initialized(object sender, EventArgs e)
        {
            myCanvas = sender as Canvas;
            _buildingBuilder = new OwlBuildingBuilder();
            _manager = new OwlBuilderManager(_buildingBuilder);
            _parameter = new BuildingParameter();
            _elevatorSelectionService = new OwlElevatorSelectionService();
            _owlElevatorValidationService = new OwlElevatorValidationService();
            _owlFlourPersonService = new OwlFlourPersonService(_elevatorSelectionService, _owlElevatorValidationService);
            _parameter.FlourCount = 6;
            _parameter.ElevatorCount = 2;
            DrawComponents();
            SetTimer();

        }

        private void MoveElevatorToFloor(int elevatorId)
        {
            var elevator = _building.Elevators.FirstOrDefault(x => x.Id == elevatorId);
            elevator.NextFlour = SelectElevatorDirection(elevator);
            DoubleAnimation animation = new DoubleAnimation();
            animation.To = 150.0 + (elevator.NextFlour.Value) * 100.0;
            animation.From = (double)elevators[elevatorId].GetValue(Canvas.TopProperty);
            //animation.Duration =
            //    TimeSpan.FromSeconds(_building.MaxFlour * 2 * Math.Abs(elevator.CurrentFlour.Value - elevator.NextFlour.Value) / (double)_building.MaxFlour);

            animation.Duration = TimeSpan.FromSeconds(2);

            animation.Completed += (sender, eArgs) =>
            {

                elevator.CurrentFlour = elevator.NextFlour;
                RemovePersonsFromElevator(elevator);
                MoveElevatorToFloor(elevatorId);
                //MovePersonIntoElevator(elevator);
            };
            elevators[elevatorId].BeginAnimation(Canvas.TopProperty, animation);
        }

        private void RemovePersonsFromElevator(IElevator<int, Guid> elevator)
        {
            var stayPeople = elevator.People.Where(x => x.OutFlour != elevator.CurrentFlour);
            var goAwayPeople = elevator.People.Where(x => x.OutFlour == elevator.CurrentFlour);
            elevator.People = stayPeople.ToList();
            foreach (var item in goAwayPeople)
            {
                elevator.People.Remove(item);
                GoAwayFromElevator(item, elevator.Id);
            }
        }
        private void MovePersonToElevator(Resources.Person person, int elvatorId)
        {
            DoubleAnimation animation = new DoubleAnimation();
            animation.From = (double)person.GetValue(Canvas.RightProperty);
            animation.To = this.Width - 300.0 * _building.Elevators.Count() - person.Width * this.People.Count(x => x.InFlour == person.InFlour);
            animation.Duration = TimeSpan.FromSeconds(2);
            animation.Completed += (a, b) => MoveElevatorToFloor(elvatorId);
            person.BeginAnimation(Canvas.RightProperty, animation);
        }
        private void MovePersonIntoElevator(IElevator<int, Guid> elevator)
        {
            _building.Flours.FirstOrDefault(x => x.Number == elevator.CurrentFlour)
                .ElevatorsQueue.Where(x => x.ElevatorId == elevator.Id);
        }
        private void GoAwayFromElevator(IPerson<Guid> person, int elevatorId)
        {
            var uiPerson = new Resources.Person();
            uiPerson.Width = 80;
            uiPerson.Height = 80;
            uiPerson.Id = person.Id;
            uiPerson.SetValue(Canvas.RightProperty, this.Width - 350.0 * _building.Elevators.Count());
            uiPerson.SetValue(Canvas.TopProperty, 150.0 + person.OutFlour * 100.0);
            myCanvas.Children.Add(uiPerson);

            DoubleAnimation animation = new DoubleAnimation();
            animation.From = (double)elevators[elevatorId].GetValue(Canvas.LeftProperty);
            animation.To = this.Width + 50;
            animation.Duration = TimeSpan.FromSeconds(4);
            animation.Completed += (x, y) => myCanvas.Children.Remove(uiPerson);
            uiPerson.BeginAnimation(Canvas.LeftProperty, animation);
        }
        //private void Button_Click(object sender, RoutedEventArgs e)
        //{
        //    MoveElevatorToFloor(rnd.Next(0, ElevatorsCount), rnd.Next(0, FloorCount));
        //}       

        private void SetTimer()
        {

            // Create a timer with a 3 second interval.
            _timer = new System.Timers.Timer(7000);
            // Hook up the Elapsed event for the timer. 
            _timer.Elapsed += OnTimedEvent;
            _timer.AutoReset = true;
            _timer.Enabled = true;
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            var personElevator = _owlFlourPersonService.CreatePerson(ref _building);
            Dispatcher.Invoke(() =>
            {
                CreateUIPerson(personElevator.Person, personElevator.ElevatorId);
                //MoveElevatorToFloor(personElevator.ElevatorId);
            });
        }

        private int SelectElevatorDirection(IElevator<int, Guid> elevator)
        {
            int nextFlour = -1;
            var people = new List<IPerson<Guid>>();
            foreach (var person in elevator.People)
            {
                people.Add(new Person(person));
            }

            if (people.Any())
            {
                var nextFlourPeople = people.Where(x => x.Direction == elevator.Direction);
                if (elevator.Direction == DirectionType.Up)
                {
                    nextFlour = nextFlourPeople.Any(x => x.OutFlour >= elevator.CurrentFlour && x.Direction == elevator.Direction) ?
                        GetMin(nextFlourPeople.Where(x => x.OutFlour >= elevator.CurrentFlour && x.Direction == elevator.Direction).Select(x => x.OutFlour))
                        : nextFlourPeople.FirstOrDefault()?.OutFlour ?? -1;
                }
                else if (elevator.Direction == DirectionType.Down)
                {
                    nextFlour = nextFlourPeople.Any(x => x.OutFlour <= elevator.CurrentFlour && x.Direction == elevator.Direction)
                        ? GetMax(nextFlourPeople.Where(x => x.OutFlour <= elevator.CurrentFlour && x.Direction == elevator.Direction).Select(x => x.OutFlour))
                        : nextFlourPeople.FirstOrDefault()?.OutFlour ?? -1;
                }
            }

            var queuePeople = this._building.Flours.SelectMany(x => x.ElevatorsQueue).Where(x => x.ElevatorId == elevator.Id);

            if (queuePeople.Any())
            {
                if (elevator.Direction == DirectionType.Up
                    && queuePeople.Where(x => x.Person.InFlour > elevator.CurrentFlour && x.Person.InFlour < nextFlour).Any())
                {
                    nextFlour = queuePeople
                        .Where(x => x.Person.InFlour > elevator.CurrentFlour && x.Person.InFlour < nextFlour).Min(x => x.Person.InFlour);
                    foreach (var item in queuePeople
                        .Where(x => x.Person.InFlour > elevator.CurrentFlour && x.Person.InFlour == nextFlour))
                    {
                        elevator.People.Add(item.Person);
                        queuePeople.ToList().Remove(item);
                        RemoveUiPersonFromQueue(item.Person);
                    }
                }
                if (elevator.Direction == DirectionType.Down
                    && queuePeople.Where(x => x.Person.InFlour < elevator.CurrentFlour && x.Person.InFlour > nextFlour).Any())
                {
                    nextFlour = queuePeople
                        .Where(x => x.Person.InFlour < elevator.CurrentFlour && x.Person.InFlour > nextFlour).Max(x => x.Person.InFlour);
                    foreach (var item in queuePeople
                       .Where(x => x.Person.InFlour < elevator.CurrentFlour && x.Person.InFlour == nextFlour))
                    {
                        elevator.People.Add(item.Person);
                        queuePeople.ToList().Remove(item);
                        RemoveUiPersonFromQueue(item.Person);
                    }
                }
            }

            if (nextFlour == -1 && queuePeople.Any())
            {
                var personElevator = queuePeople.FirstOrDefault();
                nextFlour = personElevator.Person.InFlour;
                elevator.People.Add(personElevator.Person);
                elevator.Direction = personElevator.Person.Direction;
                queuePeople.ToList().Remove(personElevator);
                RemoveUiPersonFromQueue(personElevator.Person);
            }

            return nextFlour == -1 ? elevator.CurrentFlour.Value - 1 : nextFlour;
        }

        private void CreateUIPerson(IPerson<Guid> person, int elervatorId)
        {
            var uiPerson = new Resources.Person();
            uiPerson.Width = 80;
            uiPerson.Height = 80;
            uiPerson.Id = person.Id;
            uiPerson.InFlour = person.InFlour;
            uiPerson.OutFlour = person.OutFlour;
            uiPerson.Direction = person.Direction;
            uiPerson.SetValue(Canvas.RightProperty, 10.0);
            uiPerson.SetValue(Canvas.TopProperty, 150.0 + person.InFlour * 100.0);
            myCanvas.Children.Add(uiPerson);
            this.People.Add(uiPerson);
            MovePersonToElevator(uiPerson, elervatorId);
        }

        private void RemoveUiPersonFromQueue(IPerson<Guid> person)
        {
            var uiPerson = People.FirstOrDefault(x => x.Id == person.Id);
            myCanvas.Children.Remove(uiPerson);
            People.Remove(uiPerson);
        }

        private int GetMin(IEnumerable<int> digits)
        {
            if (!digits.Any())
                return -1;

            var min = digits.FirstOrDefault();
            foreach (var digit in digits)
            {
                min = min > digit ? digit : min;
            }

            return min;
        }

        private int GetMax(IEnumerable<int> digits)
        {
            if (!digits.Any())
                return -1;

            var max = digits.FirstOrDefault();
            foreach (var digit in digits)
            {
                max = max < digit ? digit : max;
            }

            return max;
        }
    }
}
